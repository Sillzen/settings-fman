# fman settings

[https://fman.io/docs/data-directory](https://fman.io/docs/data-directory)

Windows directory: `\Users\user\AppData\Roaming\fman\plugins\User\`

OSX directory: `~/Library/Application Support/fman/Plugins/User/`

Ubuntu directory: `~/.config/fman/Plugins/User/`

Regardless of where the settings are located, the directory can be accessed via fman by opening the command palette (Ctrl+Shift+P), locating "List Plugins" and then choosing "Settings".

## What each file is for

* `Key Bindings.json` - set the key bindings.
* `Key Bindings (*).json` - OS specific settings.
* `Core Settings.json` - set the external file manager, terminal and text editor applications to use.
* `Core Settings (*).json` - OS specific settings.
* `Theme.css` - CSS styles for elements of the interface.
* `Theme (*).css` - OS specific themes.
